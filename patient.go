package main

import "fmt"

type Patient struct {
	id          int
	name        string
	vorname     string
	adresse     string
	medikamente MedikamentenListe
}

// Erwartet einen Kunden und liefert dazu einen String.
// "Methode" des Datentyps Patient.
func (p Patient) String() string {
	formatString := "ID: %v\n" +
		"Vorname: %v\n" +
		"Name: %v\n" +
		"Adresse: %v\n" +
		"Medikamente: %v\n"
	result := fmt.Sprintf(
		formatString,
		p.id,
		p.vorname,
		p.name,
		p.adresse,
		p.medikamente.String())
	// fmt.Sprintf() ist wie fmt.Printf(), liefert aber einen String, statt auf die Konsole zu schreiben.

	return result
}

// Liefert die Nebenwirkungen eines Patienten als Liste von Strings.
func (p Patient) Nebenwirkungen() []string {
	result := make([]string, 0)

	for _, m := range p.medikamente {
		for _, n := range m.nebenwirkungen {
			if !contains(result, n) {
				result = append(result, n)
			}
		}
	}

	return result
}

func MakePatient(id int, name, vorname string) Patient {
	return Patient{id, name, vorname, "Keine Adresse angegeben", make(MedikamentenListe, 0)}
}
