package main

// Struct für eine Arztdatenbank. Enthält eine Patientenliste und die höchste bisher vergebene ID.
type ArztDatenbank struct {
	maxId     int
	patienten []Patient
}

// Erzeugt einen neuen Patienten mit neuer ID und dem gegebenen Namen und Vornamen. Fügt ihn zur Liste hinzu und aktualisiert d.maxId.
func (d *ArztDatenbank) NeuerPatient(name, vorname string) int {
	p := MakePatient(d.maxId+1, name, vorname)
	d.patienten = append(d.patienten, p)
	d.maxId = p.id
	return p.id
}

// Liefert den Patienten mit der gegebenen id aus der Datenbank, falls ein solcher existiert.
// Anderenfalls wird ein ungültiger Patient mit der id -1 geliefert.
func (d ArztDatenbank) Patient(id int) Patient {
	for _, p := range d.patienten {
		if p.id == id {
			return p
		}
	}
	return MakePatient(-1, "", "")
}

// Fügt eine Adresse zum Patienten mit der gegebenen ID hinzu.
// Hat keinen Effekt, falls die ID nicht in der Datenbank existiert.
func (d *ArztDatenbank) AdresseHinzufuegen(id int, adresse string) {
	for i := range d.patienten {
		if id == d.patienten[i].id {
			d.patienten[i].adresse = adresse
		}
	}
}

// Fügt ein Medikament zum Patienten mit der gegebenen ID hinzu.
// Hat keinen Effekt, falls die ID nicht in der Datenbank existiert.
func (d *ArztDatenbank) MedikamentHinzufuegen(id int, medi Medikament) {
	for i := range d.patienten {
		if id == d.patienten[i].id {
			d.patienten[i].medikamente = append(d.patienten[i].medikamente, medi)
		}
	}
}
