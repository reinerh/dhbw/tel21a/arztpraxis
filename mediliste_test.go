package main

import "fmt"

func ExampleMedikamentenliste() {
	aspirin := Medikament{"Aspirin", []string{"Magengeschwüre"}}
	ibu := Medikament{"Ibuprofen", []string{"Nierenversagen"}}

	mediliste1 := MedikamentenListe{aspirin, ibu}
	mediliste2 := MedikamentenListe{}

	fmt.Println(mediliste1)
	fmt.Println(mediliste2)

	// Output:
	// Aspirin, Ibuprofen
	// Keine Medikamente
}
