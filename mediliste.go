package main

type MedikamentenListe []Medikament

func (l MedikamentenListe) NamensStrings() []string {
	result := make([]string, 0)

	for _, v := range l {
		result = append(result, v.String())
	}

	return result
}

func (l MedikamentenListe) NebenwirkungsStrings() []string {
	result := make([]string, 0)

	for _, v := range l {
		if v.HatNebenwirkungen() {
			result = append(result, v.Nebenwirkungen())
		}
	}

	return result
}

func (l MedikamentenListe) String() string {
	return concatDefault(l.NamensStrings(), "Keine Medikamente")
}

func (l MedikamentenListe) Nebenwirkungen() string {
	return concatDefault(l.NebenwirkungsStrings(), "Keine Nebenwirkungen")
}
