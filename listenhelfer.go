package main

// Erwartet eine Liste von Strings.
// Liefert diese Liste als einen String, wobei alle Elemente
// mit ", " getrennt sind.
// Ist die Liste leer, wird def eingesetzt.
func concatDefault(list []string, def string) string {
	result := ""

	for _, element := range list {
		result += element + ", "
	}
	if len(result) == 0 {
		result = def
	} else {
		result = result[:len(result)-2]
	}
	return result
}

// Erwartet eine Liste von Strings und einen String.
// Liefert true, falls der String in der Liste enthalten ist.
func contains(list []string, s string) bool {
	if len(list) == 0 {
		return false
	}
	if list[0] == s {
		return true
	}
	return contains(list[1:], s)
}
