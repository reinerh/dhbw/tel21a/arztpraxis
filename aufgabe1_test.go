package main

import "fmt"

func ExampleMedikamentNebenwirkungen() {
	aspirin := Medikament{"Aspirin", []string{"Magengeschwüre", "Durchfall"}}
	ibu := Medikament{"Ibuprofen", []string{"Nierenversagen"}}
	vitaminc := Medikament{"Vitamin C", []string{}}

	fmt.Println(aspirin.Nebenwirkungen())
	fmt.Println(ibu.Nebenwirkungen())
	fmt.Println(vitaminc.Nebenwirkungen())

	// Output:
	// Magengeschwüre, Durchfall
	// Nierenversagen
	// Keine Nebenwirkungen
}

func ExampleMedilisteNebenwirkungen() {
	aspirin := Medikament{"Aspirin", []string{"Magengeschwüre", "Durchfall"}}
	ibu := Medikament{"Ibuprofen", []string{"Nierenversagen"}}
	vitaminc := Medikament{"Vitamin C", []string{}}
	vitamind := Medikament{"Vitamin D", []string{}}

	mediliste1 := MedikamentenListe{aspirin, ibu}
	mediliste2 := MedikamentenListe{}
	mediliste3 := MedikamentenListe{vitaminc}
	mediliste4 := MedikamentenListe{vitaminc, vitamind}
	mediliste5 := MedikamentenListe{vitaminc, aspirin}

	fmt.Println(mediliste1.Nebenwirkungen())
	fmt.Println(mediliste2.Nebenwirkungen())
	fmt.Println(mediliste3.Nebenwirkungen())
	fmt.Println(mediliste4.Nebenwirkungen())
	fmt.Println(mediliste5.Nebenwirkungen())

	// Output:
	// Magengeschwüre, Durchfall, Nierenversagen
	// Keine Nebenwirkungen
	// Keine Nebenwirkungen
	// Keine Nebenwirkungen
	// Magengeschwüre, Durchfall
}
