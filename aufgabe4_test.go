package main

import "fmt"

func ExampleArztDatenbank() {

	d1 := ArztDatenbank{}

	id1 := d1.NeuerPatient("Mustermann", "Max")

	p1 := d1.Patient(id1)
	fmt.Println(p1)

	// Output:
	// ID: 1
	// Vorname: Max
	// Name: Mustermann
	// Adresse: Keine Adresse angegeben
	// Medikamente: Keine Medikamente

}
