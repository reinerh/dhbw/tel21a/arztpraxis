package main

import "fmt"

func ExamplePatient() {
	aspirin := Medikament{"Aspirin", []string{"Magengeschwüre"}}
	ibu := Medikament{"Ibuprofen", []string{"Nierenversagen"}}

	p1 := Patient{
		1,
		"Mustermann",
		"Max",
		"Musterstr. 1, 12345 Musterhausen",
		MedikamentenListe{aspirin},
	}
	p2 := Patient{
		2,
		"Beispiel",
		"Barbara",
		"Musterstr. 2, 34567 Musterheim",
		MedikamentenListe{ibu},
	}
	p3 := Patient{
		3,
		"Musterfrau",
		"Monika",
		"Musterstr. 3, 56789 Musterstadt",
		MedikamentenListe{},
	}

	// Diese beiden Zeilen machen exakt das gleiche.
	fmt.Println(p1.String())
	fmt.Println(p1)

	fmt.Println(p2)
	fmt.Println(p3)

	// Output:
	// ID: 1
	// Vorname: Max
	// Name: Mustermann
	// Adresse: Musterstr. 1, 12345 Musterhausen
	// Medikamente: Aspirin
	//
	// ID: 1
	// Vorname: Max
	// Name: Mustermann
	// Adresse: Musterstr. 1, 12345 Musterhausen
	// Medikamente: Aspirin
	//
	// ID: 2
	// Vorname: Barbara
	// Name: Beispiel
	// Adresse: Musterstr. 2, 34567 Musterheim
	// Medikamente: Ibuprofen
	//
	// ID: 3
	// Vorname: Monika
	// Name: Musterfrau
	// Adresse: Musterstr. 3, 56789 Musterstadt
	// Medikamente: Keine Medikamente
}
