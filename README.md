# Datenbank für eine Arztpraxis

Dieses Repo soll beispielhaft den Umgang mit selbstdefinierten Datentypen in Go zeigen.
Dies geschieht am Beispiel einer Arztpraxis, die Daten zu Patienten, Medikamenten etc,
verwaltet.

## Aufgaben

### Aufgabe 1: Funktionen zur Ausgabe von Nebenwirkungen

Implementieren Sie die Funktionen zur Ausgabe von Nebenwirkungen,
die in den Dateien `mediliste.go` und `medikament.go` vorgegeben sind.

**Hinweise:**
* Die beiden Funktionen erweitern die Datentypen `MedikamentenListe` und `Medikament`
jeweils um eine Funktion `Nebenwirkungen()`.
* Sie müssen die Testfunktionen in der Datei `aufgabe1_test.go` zum Laufen bringen.

### Aufgabe 2: Refaktorierung des Datentyps `Medikament`

Bauen Sie den Datentyp `Medikament` so um, dass er für die Nebenwirkungen anstelle
eines Strings eine Liste von Strings enthält.

**Hinweise:**
* Die bisherigen Tests (inkl. `aufgabe1_test.go`) müssen weiterhin funktionieren.
* U.U. ist es sinnvoll, einen separaten Datentyp `Nebenwirkungen` als Liste von Strings
  zu definieren und für diesen eine Methode `String()` zu schreiben.

### Aufgabe 3: Auflisten aller Nebenwirkungen eines Patienten

Erweitern Sie den Datentyp `Patient` um eine Methode `Nebenwirkungen()`, die alle
Nebenwirkungen eines Patienten als Liste liefert.

**Hinweise:**
* Je nachdem, ob Sie in Aufgabe 2 einen Datentyp für Nebenwirkungen definiert haben,
  sollten Sie diesen auch hier verwenden.
* Schreiben Sie geeignete Tests für diese Funktion.
* Die Auflistung der Nebenwirkungen soll Duplikate vermeiden.
  D.h. falls eine Nebenwirkung bei mehreren Medikamenten auftritt,
  soll diese nur einmal in der Liste vorkommen.

### Aufgabe 4: Datentyp für die Arztpraxis-Datenbank

Definieren Sie einen Datentyp für die gesamte Datenbank eine Arztpraxis.

**Anforderungen:**

* Die Datenbank soll eine Liste von Patienten enthalten.
* Die Datenbank soll eine Methode enthalten, die einen neuen Patienten hinzufügt.
  * Diese Methode soll lediglich den Namen des Patienten erwarten und dann
    intern ein neues Patient-Objekt erzeugen und zur Datenbank hinzufügen.
  * Diese Methode soll selbständig eine eindeutige ID
    für diesen Patienten bestimmen und diese zurückliefern.

**Hinweise:**

* Bringen Sie die Tests in der Datei `aufgabe4_test.go` zum Laufen.
  * Die Tests sind im Moment auskommentiert,
    da das Paket ansonsten nicht compiliert werden könnte.

### Aufgabe 5: Eingabe von weiteren Patientendaten

Erweitern Sie die Arzpraxis-Datenbank, so dass sie es ermöglicht,
den Datensatz eines Patienten zu erweitern.

Es soll Funktionen geben, die ...

* ... eine ID und eine Adresse erwarten und die Adresse im entsprechenden Datensatz ergänzen.
* ... eine ID und ein Medikament erwarten und das Medikament im entsprechenden Datensatz ergänzen.

### Aufgabe 6: Unverträglichkeiten

Erweitern Sie die Datenbank, so dass sie auch eine Liste von Medikamenten-Unverträglichkeiten
enthält. Es soll eine Funktion geben, die prüft, ob einem bestimmten Patienten ein Medikament
verschrieben werden kann, oder ob dies aufgrund der bisherigen Medikamente nicht ratsam wäre.