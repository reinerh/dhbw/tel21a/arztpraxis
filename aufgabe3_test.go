package main

import "fmt"

func ExamplePatientNebenwirkungen() {
	aspirin := Medikament{"Aspirin", []string{"Magengeschwüre", "Durchfall"}}
	paracetamol := Medikament{"Ibuprofen", []string{"Durchfall"}}

	p1 := Patient{
		1,
		"Mustermann",
		"Max",
		"Musterstr. 1, 12345 Musterhausen",
		MedikamentenListe{aspirin, paracetamol},
	}

	fmt.Println(p1.Nebenwirkungen())

	// Output:
	// [Magengeschwüre Durchfall]
}
