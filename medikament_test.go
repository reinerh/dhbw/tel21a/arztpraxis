package main

import "fmt"

func ExampleMedikament() {
	aspirin := Medikament{"Aspirin", []string{"Magengeschwüre"}}
	ibu := Medikament{"Ibuprofen", []string{"Nierenversagen"}}

	fmt.Println(aspirin)
	fmt.Println(ibu)

	// Output:
	// Aspirin
	// Ibuprofen
}
