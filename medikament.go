package main

type Medikament struct {
	name           string
	nebenwirkungen []string
}

func (m Medikament) String() string {
	return m.name
}

func (m Medikament) Nebenwirkungen() string {
	return concatDefault(m.nebenwirkungen, "Keine Nebenwirkungen")
}

func (m Medikament) HatNebenwirkungen() bool {
	return len(m.nebenwirkungen) != 0
}
